package balam.app.examen.ui.Activity.listaCliente;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.mikelau.views.shimmer.ShimmerRecyclerViewX;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import balam.app.examen.BuildConfig;
import balam.app.examen.R;
import balam.app.examen.tools.Common;
import balam.app.examen.ui.Activity.mainActivity.MainActivity;
import balam.app.examen.ui.Activity.mapsActivity.MapsActivity;
import balam.app.examen.ui.Activity.registro.RegistroAcivity;

public class ListaCliente extends AppCompatActivity {
    ClienteAdapter mClienteAdapter;
    FirebaseFirestore mFirestore;
    Button btnCliente;
    private ShimmerRecyclerViewX shimmerRecycler;
    private static final int REQUEST_PERMISSION_CODE = 5656;
    private static final String FILE_ZIP = "employees_data.json.zip";
    private static final String FILE_JSON = "employees_data.json";
    String Cambios;
    Boolean descomprimir = false;
    SharedPreferences prefs;

    private static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_cliente);
        btnCliente = findViewById(R.id.btn_cliente);
        btnCliente.setOnClickListener(view -> startActivity(new Intent(ListaCliente.this, MapsActivity.class)));
        mFirestore = FirebaseFirestore.getInstance();
        Query query = mFirestore.collection(Common.COMMON_NEWUSER);
        FirestoreRecyclerOptions<ClientePojo> firestoreRecyclerOptions = new FirestoreRecyclerOptions
                .Builder<ClientePojo>().setQuery(query, ClientePojo.class).build();
        mClienteAdapter = new ClienteAdapter(firestoreRecyclerOptions);
        mClienteAdapter.notifyDataSetChanged();
        shimmerRecycler = findViewById(R.id.shimmer_recycler_view);
        shimmerRecycler.setLayoutManager(new LinearLayoutManager(this));
        shimmerRecycler.setAdapter(mClienteAdapter);
        shimmerRecycler.showShimmerAdapter();
        shimmerRecycler.hideShimmerAdapter();
        if (descomprimir == false){
            verifyStoragePermissions();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();
        mClienteAdapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mClienteAdapter.stopListening();

    }

    private void processZip() {
        // Archivo zip en la carpeta de descarga del sistema
        File downloadFolder = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        File zipFile = new File(downloadFolder, FILE_ZIP);
        // Carpeta privada de aplicaciones en un disco externo (tarjeta de memoria) dentro de 'documentos'
        File externalFilesDir = getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS);
        int countFiles = unZip(zipFile, externalFilesDir);
        Toast.makeText(this,"Archivos descomprimidos " + countFiles, Toast.LENGTH_LONG).show();
        SaveIntoSharedPreferences("descomprimir",true);

        new VersionCheck().execute();
    }


    private int unZip(File zipFile, File folderToExtract) {

        if(!zipFile.exists()) {
            Toast.makeText(this,"Zip no existe ", Toast.LENGTH_SHORT).show();
            return 0;
        }

        try  {

            int totalFilesInZip = countFiles(zipFile);
            int countFiles = 0;

            ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFile));
            ZipEntry zipEntry;
            byte[] buffer = new byte[1024];
            int count;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                if(zipEntry.isDirectory()) {

                    File f = new File(folderToExtract.getPath() + zipEntry.getName());
                    if(!f.isDirectory()) {
                        f.mkdirs();
                    }

                } else {
                    ++countFiles;
                    Log.v(TAG, "Descomprimiendo " + " Archivo " + countFiles + "/" + totalFilesInZip + " Name: " + zipEntry.getName());
                    File fileOut = new File(folderToExtract, zipEntry.getName());
                    FileOutputStream fileOutputStream = new FileOutputStream(fileOut);
                    while ((count = zipInputStream.read(buffer)) != -1) {
                        fileOutputStream.write(buffer, 0, count);
                    }
                    zipInputStream.closeEntry();
                    fileOutputStream.close();
                }
            }
            zipInputStream.close();
            return countFiles;

        } catch(Exception e) {
            Log.e(TAG, "Descomprimir archivo " + zipFile.getName(), e);
            return 0;
        }

    }

    // to show progress
    private int countFiles(File zipFile) throws IOException {
        ZipFile xzipFile = new ZipFile(zipFile);
        final Enumeration<? extends ZipEntry> entries = xzipFile.entries();
        int numRegularFiles = 0;
        while (entries.hasMoreElements()) {
            if (! entries.nextElement().isDirectory()) {
                ++numRegularFiles;
            }
        }
        return numRegularFiles;
    }


    // verify permission
    private void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION_CODE);
        } else {
            processZip();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted! Do the work unzip!!
                    processZip();
                } else {
                    Toast.makeText(this, "No puedo descomprimir. Por favor dame permisos", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    private class VersionCheck extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();
            String jsonStr = sh.makeServiceCall(FILE_JSON);
            if (jsonStr != null){
                try {

                    JSONObject jsonObj = new JSONObject(jsonStr);
                    JSONArray obtener = jsonObj.getJSONArray("employees");
                    for (int i = 0; i < obtener.length(); i++)
                    {
                        JSONObject v = obtener.getJSONObject(i);
                        String id = obtener.getString(Integer.parseInt("id"));
                        String name = obtener.getString(Integer.parseInt("name"));
                        String latitud = obtener.getString(Integer.parseInt("lat"));
                        String longitud = obtener.getString(Integer.parseInt("log"));
                        String email = obtener.getString(Integer.parseInt("mail"));

                        Map<String, Object> map = new HashMap<>();
                        map.put("id", id);
                        map.put("nombre",name);
                        map.put("latitud", latitud);
                        map.put("longitud", longitud);
                        map.put("email", email);

                        mFirestore.collection(Common.COMMON_NEWUSER).add(map).addOnSuccessListener(documentReference -> {

                        }).addOnFailureListener(e ->
                                Toast.makeText(ListaCliente.this, "Falló el registro.", Toast.LENGTH_SHORT).show()
                        );

                    }
                }catch (final JSONException e) {

                    // Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            Toast.makeText(getApplicationContext(),
                                    "El formato de JSON esta errado: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                //Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "El servidor de comprobar versión esta caido.",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }
            return null;
        }

        @Override
        protected void onPostExecute (Void result){

                super.onPostExecute(result);
                String VersionName = BuildConfig.VERSION_NAME;
                if (Cambios!=null){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ListaCliente.this);
                    builder.setTitle("Actualización Completada");
                    builder.setIcon(R.mipmap.ic_launcher);
                    builder.setMessage("Se guardo los datos en frireStore");

                    AlertDialog alert = builder.create();
                    alert.show();
                }
        }
    }

    private  void SaveIntoSharedPreferences(String key, boolean value){
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
}

