package balam.app.examen.tools;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import balam.app.examen.model.UserModel;


public class Common {
    public static final String COMMON_USER = "usuarios";
    public static final String COMMON_NEWUSER = "listUser";
    public static UserModel currentPerfil;

    public static String buildWelcomeMessage() {
        if (balam.app.examen.tools.Common.currentPerfil != null){
            return new StringBuilder("Bienvenido \n")
                    .append(balam.app.examen.tools.Common.currentPerfil.getEdtName()+" "+ balam.app.examen.tools.Common.currentPerfil.getEdtApellido())
                    .append(" ").toString();
        }
        else
            return "";

    }

    public static void showNotification(Context contex, int id, String title, String body, Intent intent) {
        PendingIntent pendingIntent = null;
        if (intent != null)
            pendingIntent = PendingIntent.getActivity(contex,id,intent, PendingIntent.FLAG_UPDATE_CURRENT);
        String NOTIFICATION_CHANNEL_ID = "Emamen NPBA";
        NotificationManager notificationManager = (NotificationManager)contex.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    "Emamen NPBA", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.setDescription("Emamen NPBA");
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.setVibrationPattern(new long[]{0,1000,500,1000});
            notificationChannel.enableVibration(true);

            notificationManager.createNotificationChannel(notificationChannel);
        }
        NotificationCompat.Builder builder = new NotificationCompat.Builder(contex,NOTIFICATION_CHANNEL_ID);
        builder.setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(false)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(Notification.DEFAULT_VIBRATE);
        if (pendingIntent != null)
        {
            builder.setContentIntent(pendingIntent);
        }
        Notification notification = builder.build();
        notificationManager.notify(id,notification);
    }


}
