package balam.app.examen.ui.Activity.login;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.ui.auth.AuthMethodPickerLayout;
import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import balam.app.examen.ui.Activity.mainActivity.MainActivity;
import balam.app.examen.R;
import balam.app.examen.model.UserModel;
import balam.app.examen.tools.Common;
import balam.app.examen.ui.base.BaseActivity;

public class LoginActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private static final int MY_REQUEST_CODE = 7117;
    Button btnRegister, sesion, googleSignIn;
    TextInputEditText passTextEmail;
    TextInputEditText passTextPassword;
    ConstraintLayout contentLogin;
    FirebaseFirestore mFirestore;

    @Override
    protected void onStart(){
        super.onStart();

        FirebaseUser currentUser = mAuth.getCurrentUser();
        if (currentUser != null) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
            Toast.makeText(this, "Bienvenido nuevmente " + currentUser.getDisplayName(), Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Por favor Iniciar sesión", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        onClick();
    }

    private void init() {
        mAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();
        btnRegister = findViewById(R.id.btn_register);
        googleSignIn = findViewById(R.id.btn_google_sign_in);
        sesion = findViewById(R.id.sesion);
        passTextEmail = findViewById(R.id.LoginEmail);
        passTextPassword = findViewById(R.id.LoginPass);
        contentLogin = findViewById(R.id.container);


    }
    private void onClick() {
        btnRegister.setOnClickListener(view -> showRegisterLayout());
        sesion.setOnClickListener(view -> validateEmailAndPAssword(passTextEmail, passTextPassword));
        googleSignIn.setOnClickListener(view ->  showSignInOptions());
    }

    private boolean validateEmailAndPAssword(EditText email, EditText password){
        String emailInput = email.getText().toString();
        String passWordInput = password.getText().toString();

        if (!passWordInput.isEmpty() && !emailInput.isEmpty()){
            if (Patterns.EMAIL_ADDRESS.matcher(emailInput).matches()){
                LoginOne();
            }else {
                showSnackbarShort(getString(R.string.email_incorrecto));
            }
            return true;
        }else {
            showSnackbarShort(getString(R.string.datos_faltantes));
            return false;
        }

    }
    private void LoginOne() {
        mAuth.signInWithEmailAndPassword(passTextEmail.getText().toString(), passTextPassword.getText().toString())
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                    } else {
                        Log.w("Hola", "iniciar sesión con correo electrónico: error", task.getException());
                        Toast.makeText(LoginActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    private void showRegisterLayout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.DialogTheme);
        View itemView = LayoutInflater.from(this).inflate(R.layout.layout_register, null);

        TextInputEditText editRegistroNombre = itemView.findViewById(R.id.editNombre);
        TextInputEditText editRegistroApellidos = itemView.findViewById(R.id.editApellidos);
        TextInputEditText editRegistroTextEmail = itemView.findViewById(R.id.editTextEmail);
        TextInputEditText editRegistroTextPassword = itemView.findViewById(R.id.editPassword);
        TextInputEditText editRegistrofecha = itemView.findViewById(R.id.editfecha);
        Button btnContinue = itemView.findViewById(R.id.register_user);

        builder.setView(itemView);
        AlertDialog dialog = builder.create();
        dialog.show();


        btnContinue.setOnClickListener(view -> {
            mAuth.createUserWithEmailAndPassword(editRegistroTextEmail.getText().toString(), editRegistroTextPassword.getText().toString())
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()){
                            UserModel usermodel = new UserModel();
                            usermodel.setEdtName(editRegistroNombre.getText().toString());
                            usermodel.setEdtApellido(editRegistroApellidos.getText().toString());
                            usermodel.setEdtEmail(editRegistroTextEmail.getText().toString());
                            usermodel.setEdtNacimiento(editRegistrofecha.getText().toString());

                            String nombre = editRegistroNombre.getText().toString();
                            String apellido = editRegistroApellidos.getText().toString();
                            String email = editRegistroTextEmail.getText().toString();
                            String nacimiento = editRegistrofecha.getText().toString();

                            Map<String, Object> map = new HashMap<>();
                            map.put("Nombre", nombre);
                            map.put("apellido",apellido);
                            map.put("email", email);
                            map.put("nacimiento", nacimiento);
                            mFirestore.collection(Common.COMMON_USER).add(map).addOnSuccessListener(documentReference -> {
                                Snackbar.make(contentLogin,"¡Regístro con éxito!",Snackbar.LENGTH_SHORT).show();
                                dialog.dismiss();
                                goToHomeActivity(usermodel);

                            }).addOnFailureListener(e ->
                                    Snackbar.make(contentLogin,"Falló la autenticación.",Snackbar.LENGTH_SHORT).show());

                        }else {
                            Log.w("Hola", "crear usuario con correo electrónico: error", task.getException());
                            Toast.makeText(LoginActivity.this, "Error de autenticación.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE) {
            IdpResponse response = IdpResponse.fromResultIntent(data);
            if (resultCode == RESULT_OK) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                Toast.makeText(this, "Bienvenido " + user.getDisplayName(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(LoginActivity.this, MainActivity.class));
            }
            else {
                Toast.makeText(this, "" + response.getError().getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void goToHomeActivity(UserModel userModel) {
        Common.currentPerfil =  userModel;
        startActivity(new Intent(LoginActivity.this,MainActivity.class));
        finish();
    }

    private void showSignInOptions() {
        List<AuthUI.IdpConfig> providers = new ArrayList<>();
        providers.add(new AuthUI.IdpConfig.GoogleBuilder().build());


        AuthUI.SignInIntentBuilder builder = AuthUI.getInstance().createSignInIntentBuilder();
        AuthMethodPickerLayout loginLayout = new AuthMethodPickerLayout
                .Builder(R.layout.activity_login)
                .setGoogleButtonId(R.id.btn_google_sign_in)
                .build();


        startActivityForResult(
                builder.setAuthMethodPickerLayout(loginLayout)
                        .setAvailableProviders(providers)
                        .setTheme(R.style.AppTheme)
                        .setIsSmartLockEnabled(false)
                        .build(), MY_REQUEST_CODE);
    }

}